#include "servocontrol.h"
#include "mzapo_regs.h"
#include "mzapo_phys.h"
#include <stdint.h>
#include <limits.h>

#define SERVOPS2_PWM_MIN  80000
#define SERVOPS2_PWM_MAX 200000
#define SERVOPS2_SERVO_COUNT 4

static const uint32_t servops2_pwm_offsets[SERVOPS2_SERVO_COUNT] ={
    SERVOPS2_REG_PWM1_o,
    SERVOPS2_REG_PWM2_o,
    SERVOPS2_REG_PWM3_o,
    SERVOPS2_REG_PWM4_o
};

ServoControl::ServoControl(QObject *parent) : QObject(parent)
{
   servops2_base = nullptr;
   servops2_base = map_phys_address(SERVOPS2_REG_BASE_PHYS, SERVOPS2_REG_SIZE, 0);

   if (servops2_base != nullptr) {
       *(volatile uint32_t *)(servops2_base + DCSPDRV_REG_CR_o) = 0x10f;
       *(volatile uint32_t *)(servops2_base + SERVOPS2_REG_PWMPER_o) = 1000000;
       for (int i = 0; i < SERVOPS2_SERVO_COUNT; i++) {
           if (*(volatile uint32_t *)(servops2_base + servops2_pwm_offsets[i]) < SERVOPS2_PWM_MIN)
               *(volatile uint32_t *)(servops2_base + servops2_pwm_offsets[i]) = SERVOPS2_PWM_MIN;
           if (*(volatile uint32_t *)(servops2_base + servops2_pwm_offsets[i]) > SERVOPS2_PWM_MAX)
               *(volatile uint32_t *)(servops2_base + servops2_pwm_offsets[i]) = SERVOPS2_PWM_MAX;
       }
   }
}

void ServoControl::servo_set_pos(int servo, int val)
{
    if (servo < 0 || servo >= SERVOPS2_SERVO_COUNT)
        return;
    if (val < SERVOPS2_PWM_MIN)
        val = SERVOPS2_PWM_MIN;
    if (val > SERVOPS2_PWM_MAX)
        val = SERVOPS2_PWM_MAX;
    if (servops2_base != nullptr)
            *(volatile uint32_t *)(servops2_base + servops2_pwm_offsets[servo]) = val;
}

void ServoControl::knob_set_pos(int knob, int val)
{
    double d = val;
    d /= 256;
    d *= SERVOPS2_PWM_MAX - SERVOPS2_PWM_MIN;
    d += SERVOPS2_PWM_MIN;
    if (d > INT_MAX)
        d = INT_MAX;
    if (d < INT_MIN)
        d = INT_MIN;
    servo_set_pos(knob, (int)d);
}

void ServoControl::red_knob_update(int val)
{
    knob_set_pos(0, val);
}

void ServoControl::green_knob_update(int val)
{
    knob_set_pos(1, val);
}

void ServoControl::blue_knob_update(int val)
{
    knob_set_pos(2, val);
}
