#ifndef SERVOKNOBSWINDOW_H
#define SERVOKNOBSWINDOW_H

#include <QMainWindow>

namespace Ui {
class ServoKnobsWindow;
}

class ServoKnobsWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit ServoKnobsWindow(QWidget *parent = nullptr);
    ~ServoKnobsWindow();

private:
    Ui::ServoKnobsWindow *ui;
};

#endif // SERVOKNOBSWINDOW_H
