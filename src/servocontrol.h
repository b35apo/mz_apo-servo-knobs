#ifndef SERVOCONTROL_H
#define SERVOCONTROL_H

#include <QObject>

class ServoControl : public QObject
{
    Q_OBJECT
public:
    explicit ServoControl(QObject *parent = nullptr);

signals:

public slots:
    void red_knob_update(int val);
    void green_knob_update(int val);
    void blue_knob_update(int val);
    void knob_set_pos(int knob, int val);
    void servo_set_pos(int servo, int val);

private:
    void *servops2_base;
};

#endif // SERVOCONTROL_H
