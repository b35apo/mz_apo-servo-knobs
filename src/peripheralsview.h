#ifndef PERIPHERALSVIEW_H
#define PERIPHERALSVIEW_H

#include <QWidget>

#if 0
#include "peripspiled.h"
#endif

namespace Ui {
class PeripheralsView;
}

class PeripheralsView : public QWidget
{
    Q_OBJECT

public:
    explicit PeripheralsView(QWidget *parent = nullptr);
    ~PeripheralsView();

#if 0
    void setup(const machine::PeripSpiLed *perip_spi_led);
#endif

signals:
    void red_knob_changed(int val);
    void green_knob_changed(int val);
    void blue_knob_changed(int val);

public slots:
    void led_line_changed(uint val);
    void led_rgb1_changed(uint val);
    void led_rgb2_changed(uint val);

private:
    Ui::PeripheralsView *ui;
};

#endif // PERIPHERALSVIEW_H
