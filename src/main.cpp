#include "servoknobswindow.h"
#include "peripheralsview.h"
#include "servocontrol.h"
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    ServoKnobsWindow w;

    PeripheralsView *periph_view = new PeripheralsView();
    ServoControl *servo_control = new ServoControl(periph_view);
    w.connect(periph_view, SIGNAL(red_knob_changed(int)), servo_control, SLOT(red_knob_update(int)));
    w.connect(periph_view, SIGNAL(green_knob_changed(int)), servo_control, SLOT(green_knob_update(int)));
    w.connect(periph_view, SIGNAL(blue_knob_changed(int)), servo_control, SLOT(blue_knob_update(int)));

    w.setCentralWidget(periph_view);

    w.show();

    return a.exec();
}
