#include "servoknobswindow.h"
#include "ui_servoknobswindow.h"

ServoKnobsWindow::ServoKnobsWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::ServoKnobsWindow)
{
    ui->setupUi(this);
}

ServoKnobsWindow::~ServoKnobsWindow()
{
    delete ui;
}
